from jira import JIRA
from st2client.client import Client
import requests
import json

#  from st2common.runners.base_action import Action
__all__ = [
    'BaseAction'
]


class Action(object):
    def __init__(self, config):
        self.config = config


class BaseAction(Action):
    def __init__(self, config):
        super(BaseAction, self).__init__(config=config)
        self._client = self._get_client()

    def _get_client(self):
        config = self.config
        basic_creds = (config['username'],config['password'])
        client = JIRA(options={'server': config['url']}, basic_auth=basic_creds)
        return client

    def get_field_value(self,url,auth):
        headers = {
           "Content-Type": "application/json"
        }
        response = requests.request("GET",url,headers=headers,auth=auth)
        data = json.loads(response.text)
        return data

    def add_field_value(self,url,auth,project_name):
        headers = {
            "Content-Type": "application/json"
        }
        data = '{"optionvalue": "%s"}' % project_name
        requests.post(url,headers=headers,data=data,auth=auth)

    def delete_field_value(self,url,auth,id):
        deleteUrl = url + '/' + str(id)
        requests.delete(deleteUrl,auth=auth)

    @staticmethod
    def remove_automation_label(issue):
        labels = issue.raw["fields"]["labels"]
        # print(labels)
        try:
            labels.remove("automation_in_progress")
        except Exception as e:
            pass
        issue.update(fields={'labels': labels})
