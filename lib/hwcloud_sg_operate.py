import json
import requests



class Hwcloud_sg_operation(object):
    def __init__(self, username, iamname, password):

        self.iam_endpoint = 'iam.cn-south-1.myhuaweicloud.com'
        self.vpc_endpoint = 'vpc.cn-south-1.myhuaweicloud.com'
        self.project_name = 'cn-south-1'
        self.__token = self.__get_token(self.iam_endpoint, username, iamname, password)
        self.__headers = {
                         "User-Agent": "API Explorer",
                         "x-auth-token": self.__token,
                         "Content-Type": "application/json;charset=UTF-8"
                        }

    def __get_token(self, endpoint, username, iamname, password):
        headers = {'Content-Type': 'application/json'}
        base_url = 'https://' + endpoint + '/v3/auth/tokens'
        data = {
            "auth": {
                "identity": {
                    "methods": [
                        "password"
                    ],
                    "password": {
                        "user": {
                            "domain": {
                                "name": username
                            },
                            "name": iamname,
                            "password": password
                        }
                    }
                },
                "scope": {
                    "project": {
                        "name": self.project_name
                    }
                }
            }
        }
        response = requests.post(url = base_url, headers = headers, data = json.dumps(data))
        print(response.status_code)
        if response.status_code != 201:
            raise Exception('Unknow status code, has not get the valid response')
        token = response.headers['x-subject-token']
        print('Get token successfully')
        return token

    def _check_response_status_code(self, status_code, result):
        print(status_code)
        if status_code != 200:
            result['code'] = 1
            result['msg'] = 'Unknow status code, has not get the valid response'
        return result

    def get_security_groups(self, project_id):
        url = 'https://%s/v1/%s/security-groups' % (self.vpc_endpoint, project_id)
        response = requests.get(url = url, headers = self.__headers)
        result = {'code': 0, 'msg':'', 'data':json.loads(response.text)}
        result = self._check_response_status_code(response.status_code, result)
        print(result['code'])
        return result

    def get_security_group_by_id(self, project_id, security_group_id):
        url = 'https://%s/v1/%s/security-groups/%s' % (self.vpc_endpoint, project_id, security_group_id)

        response = requests.get(url = url, headers = self.__headers)
        result = {'code': 0, 'msg':'', 'data':json.loads(response.text)}
        result = self._check_response_status_code(response.status_code, result)
        print(result['code'])
        return result

    def get_security_group_rules(self, project_id, security_group_id):
        url = 'https://%s/v1/%s/security-group-rules?security_group_id=%s' % (self.vpc_endpoint, project_id, security_group_id)
        response = requests.get(url = url, headers = self.__headers)
        result = {'code': 0, 'msg':'', 'data':json.loads(response.text)}
        result = self._check_response_status_code(response.status_code, result)
        print(result['code'])
        return result

    def create_security_group_rule_by_port(self, project_id, security_group_id, port, description = ''):
        url = 'https://%s/v1/%s/security-group-rules' % (self.vpc_endpoint, project_id)
        direction = 'ingress'
        data = {
            "security_group_rule": {
                "description": description,
                "direction": direction,
                "port_range_min": int(port),
                "ethertype": "IPv4",
                "port_range_max": int(port),
                "protocol": "tcp",
                "remote_ip_prefix": '',
                "security_group_id": security_group_id
                                    }
            }
        response = requests.post(url = url, headers = self.__headers, data = json.dumps(data))
        result = {'code': 0, 'msg':'', 'data':json.loads(response.text)}
        result = self._check_response_status_code(response.status_code, result)
        print(result['code'])
        return result

    def create_security_group_rule_by_ip(self, project_id, security_group_id, ip, description = ''):
        url = 'https://%s/v1/%s/security-group-rules' % (self.vpc_endpoint, project_id)
        direction = 'ingress'
        data = {
            "security_group_rule": {
                "description": description,
                "direction": direction,
                "ethertype": "IPv4",
                "protocol": "tcp",
                "remote_ip_prefix": ip,
                "security_group_id": security_group_id
                                    }
            }
        response = requests.post(url = url, headers = self.__headers, data = json.dumps(data))
        result = {'code': 0, 'msg':'', 'data':json.loads(response.text)}
        result = self._check_response_status_code(response.status_code, result)
        print(result['code'])
        return result

    def delete_security_group_rule_by_port(self, project_id, security_group_id, port):
        rules = self.get_security_group_rules(project_id, security_group_id)
        rule_id = ''
        result = {'code': 0, 'msg': 'Delete rules successfully', 'data': 'Port: %s' % port}
        for data in rules['data']['security_group_rules']:
            if data['port_range_min'] == port:
                rule_id = data['id']
                break
        if rule_id is '':
            result['code'] = 1
            result['msg'] = 'This rule does not exist in the security group'
            return result
        url = 'https://%s/v1/%s/security-group-rules/%s' % (self.vpc_endpoint, project_id, rule_id)
        response = requests.delete(url = url, headers = self.__headers)
        if response.status_code != 204:
            result = {'code': 1, 'msg': 'Unknown error occured', 'data': response.status_code}
        print(result['code'])
        return result










