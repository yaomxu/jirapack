import os, sys, re
from jira.client import JIRA
from st2client.client import Client
from st2reactor.sensor.base import PollingSensor


class JIRASensorForProjectAutomation(PollingSensor):

    def __init__(self, sensor_service, config=None, poll_interval=5):
        super(JIRASensorForProjectAutomation, self).__init__(sensor_service=sensor_service,
                                                                 config=config,
                                                                 poll_interval=poll_interval)

        self._jira_url = None
        self._jira_client = None
        self._jira_username = None
        self._jira_password = None
        self._poll_interval = 60
        self._project = "Paas Service Desk"
        self._jql_query = None
        self._issuetype = "Devops issue"
        self._issus_status = "In Progress"
        self._trigger_name = 'issues_tracker_for_jira_project'
        self._trigger_pack = 'jirapack'
        self._trigger_ref = '.'.join([self._trigger_pack, self._trigger_name])

    def setup(self):
        self.get_client()
        self._jql_query = 'project="%s" and issuetype="%s" and labels in ("automation_in_progress") and (status="%s" or status="%s")' % (
        self._project, self._issuetype,  "Waiting for support", "In Progress")

    def get_client(self):
        self._jira_username = self._config['username']
        self._jira_password = self._config['password']
        self._jira_url = self._config['url']
        basic_creds = (self._jira_username, self._jira_password)
        self._jira_client = JIRA(options={'server': self._jira_url}, basic_auth=basic_creds)

    def poll(self):
        self._detect_new_issues()

    def cleanup(self):
        pass

    def add_trigger(self, trigger):
        pass

    def update_trigger(self, trigger):
        pass

    def remove_trigger(self, trigger):
        pass

    def _detect_new_issues(self):
        new_issues = self._jira_client.search_issues(self._jql_query, maxResults=10, startAt=0)
        for issue in new_issues:
            if issue.raw['fields']['status']['name'] == "Waiting for support":
                comments = self._jira_client.comments(issue.key)
                if self.check_sent_approval_request(comments):
                    continue
            self._dispatch_issues_trigger(issue)

    def _dispatch_issues_trigger(self, issue):
        trigger = self._trigger_ref
        payload = {}
        payload['project'] = self._project
        payload['issue_key'] = issue.key
        payload['issue_url'] = issue.self
        payload['issue_browse_url'] = self._jira_url + '/browse/' + issue.key
        payload['fields'] = issue.raw.get('fields', {})
        self._sensor_service.dispatch(trigger, payload)



    def check_sent_approval_request(self, comments):
        for comment in comments:
            if comment.raw["author"]["name"] == "stackstorm":
                if re.search("Please waiting for approval!", comment.raw["body"], re.IGNORECASE):
                    return True
        return False