import os
import sys
import requests
import json
import urllib3
from base import BaseAction
from jira.client import JIRA


class CountAction(BaseAction):
    def run(self, issue_key):
        urllib3.disable_warnings()
        try:
            client = self._client
            issue = client.issue(issue_key)
            func = issue.raw['fields']['customfield_10218']['value']
            field_1 = issue.raw['fields']['customfield_10216']
            field_2 = issue.raw['fields']['customfield_10217']
            if func == 'add':
                result = field_1 + field_2
            elif func == 'minus':
                result = field_1 - field_2
            else:
                raise Exception('invalid func')

            return True,'The result for field1 %s field2 is : %s' % (func, result)
        except Exception as e:
            print(e)
            return False, e