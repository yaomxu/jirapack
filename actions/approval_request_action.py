# coding=utf-8
import re
from st2client.client import Client
from base import BaseAction
import sys

class AMApproveAction(BaseAction):
    def run(self, issue_key):
        try:
            client = self._client
            comments = client.comments(issue_key)
            for comment in comments:
                if  re.search("Please waiting for approval!", comment.raw['body']):
                    return False, 'admin has been requested to approve'
            print('Admin has been requested for approval')
            return True, "Please waiting for approval!"
        except Exception as e:
            print(e)
            return False,e
