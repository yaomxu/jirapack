from base import BaseAction
import re

__all__ = [
    'CommentIssueAction'
]

class CommentIssueAction(BaseAction):

    def run(self, issue_key, comment_text, error_message):
        issue = self._client.issue(issue_key)
        print(issue.fields.status.name)

        if re.search("Process failed", comment_text):
            if issue.fields.status.name == "Waiting for support" and error_message == 'admin has been requested to approve':
                exit()

            if error_message == "":
                error_message = "Some unknown errors happened"

            self._client.add_comment(issue_key, comment_text + "\n" + error_message)
            BaseAction.remove_automation_label(issue)
            self._client.transition_issue(issue, 31)

        else:
            self._client.add_comment(issue_key, comment_text + "\n" + error_message)

            if issue.fields.status.name == "Waiting for support":
                # change ticket state to W4A
                self._client.transition_issue(issue, 11)

            elif issue.fields.status.name == "In Progress":
                # Process done successfully, change ticket state to Resolved
                self._client.transition_issue(issue, 61)

