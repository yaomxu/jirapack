import re

import urllib3
from hwcloud_sg_operate import *
from base import BaseAction




class CountAction(BaseAction):
    def run(self, issue_key):
        urllib3.disable_warnings()
        try:
            # Get jira client and huawei cloud sg client
            Jiraclient = self._client
            hwclient = self._get_hw_cloud_operator()

            # get params from Jira
            project_name, security_group_name, func, ip_or_ports = self._get_params(Jiraclient, issue_key)

            # Processing params
            project_id = self.config['hw_project_id'][project_name]
            security_group_id = self.config['hw_security_group_id'][security_group_name]
            ip_or_ports = self.split_ip_or_port(ip_or_ports, func)

            for ip_or_port in ip_or_ports:
                # check whether the security group exist
                result = hwclient.get_security_group_by_id(project_id, security_group_id)
                if result['code'] != 0:
                    return False, "Security Group: '%s'  does not exist" % security_group_name

                # add rule to security group
                self.add_rule_to_sg(hwclient, project_id, security_group_id, ip_or_port, func)

            message = "Your automatic request add rules\n%s: %s to security group: %s has been processed successfully. " \
                      "Please move to Huawei cloud to view it" % (func, ip_or_ports, security_group_name)
            return True, message
        except Exception as e:
            print(e)
            return False, str(e)


    def _get_params(self, client, issue_key):
        issue = client.issue(issue_key)
        project_name = issue.raw['fields']['customfield_10211']['value']
        security_group_name = issue.raw['fields']['customfield_10212']['value']
        func = issue.raw['fields']['customfield_10213']['value']
        ip_or_ports = issue.raw['fields']['customfield_10214']

        return project_name, security_group_name, func, ip_or_ports

    def split_ip_or_port(self, ip_or_ports, func):
        ip_or_ports = ip_or_ports.split(';')

        if ip_or_ports[-1] is '':
            ip_or_ports.pop()

        if func == 'port':
            ip_or_ports = [int(port) for port in ip_or_ports]
        return ip_or_ports

    def _get_hw_cloud_operator(self):
        username = self.config['hw_username']
        iamname = self.config['hw_iamname']
        password = self.config['hw_password']
        operator = Hwcloud_sg_operation(username, iamname, password)
        return operator

    def check_ip_format(self, ip_addr):
        err_message = "Invalid ip address."
        pattern = re.compile(r'((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})(\.((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})){3}')
        if not pattern.search(ip_addr):
            return False, err_message
        return True, ''

    def port_criteria(self, ip_or_port):
        msg = 'Invalid port'
        if not isinstance(ip_or_port, int):
            return False, msg
        if ip_or_port < 1 or ip_or_port > 65535:
            return False, msg
        return True, ''

    def ip_criteria(self, ip_or_port):
        res, msg = self.check_ip_format(ip_or_port)
        if not res:
            return False, msg
        return True, ''

    def check_ip_or_port_valid(self, func, ip_or_port):
        if func == 'port':
            res, msg = self.port_criteria(ip_or_port)
        else:
            res, msg = self.ip_criteria(ip_or_port)

        if not res:
            raise Exception(msg)

    def add_rule_to_sg(self, hwclient, project_id, security_group_id, ip_or_port, func):
        self.check_ip_or_port_valid(func, ip_or_port)
        if func == 'port':
            hwclient.create_security_group_rule_by_port(project_id, security_group_id, ip_or_port)
        elif func == 'ip':
            hwclient.create_security_group_rule_by_ip(project_id, security_group_id, ip_or_port)
        else:
            raise Exception("Error fields: 'via ip or port', pls check it again in this ticket")